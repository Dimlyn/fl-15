function isEquals() {
    return arguments[0] === arguments[1];
}

function isBigger() {
    return arguments[0] > arguments[1];
}

function storeNames(...args) {
    return Array.from(args);
}

function getDifference(arg1, arg2) {
    return arg1 > arg2 ? arg1 - arg2 : arg2 - arg1;
}

function negativeCount(arr) {
    let negativeValue = 0;
    
    for (let valueInArray of arr) {
        valueInArray < 0 ? negativeValue++ : negativeValue;
    }
      
    return negativeValue;
}

function letterCount(arg1, arg2) {
    const startPos = -1;
    let position = -1;
    let letter_Count = 0;
    
    while ((position = arg1.toLowerCase().indexOf(arg2.toLowerCase(), position + 1)) !== startPos){
        ++letter_Count
    }

    return letter_Count;
}

function countPoints(arr) {
    const threePoints = 3;
    const onePoint = 1;
    let xTeamPoints = 0;

    for (let score of arr ) {
        let scoreX = +score.split(':')[0];
        let scoreY = +score.split(':')[1];
   
        if (isBigger(scoreX, scoreY)) {
            xTeamPoints += threePoints;
        }else if (isEquals(scoreX, scoreY)) {
            xTeamPoints += onePoint;
        }else {
            xTeamPoints += 0;
        }
    }

    return xTeamPoints;
}