function getAge() {
    
    let millisecDifference = Date.now() - arguments[0].getTime();
    let age = new Date(millisecDifference);
    const startingYear = 1970;

    return Math.abs(age.getUTCFullYear() - startingYear);
}

function getWeekDay(date) {
    
    const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']
    
    return days[date.getDay()];
}

function getAmountDaysToNewYear() {

    const secTomillsec = 1000;
    const secInMin = 60;
    const hoursInDay = 24;
    const oneDay = secTomillsec * secInMin * secInMin * hoursInDay;
    const today = new Date();
    const newYear = new Date(today.getFullYear() +1, 0, 1);

    return Math.ceil((newYear.getTime() - today.getTime()) / oneDay)
}