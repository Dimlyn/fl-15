const percentageDivider = 100;
const minInitialAmount = 1000;
const minNumberOfYears = 1;
const maxPercentageOfYear = 100;
const toFixedtwoDecimalPlaces = 2;

const initialAmount = +prompt('Please input your initial amount', minInitialAmount);
const numberOfYears = +prompt('Please input total number of years', minNumberOfYears);
const percentageOfYear = +prompt('Please input percentage of a year', maxPercentageOfYear);

let pecentageProfitAfterYear;
let profitPercentagePlusYearAmount;

let userInputinitialAmount = initialAmount;

for (let i = 1; i <= numberOfYears; i++) {
    pecentageProfitAfterYear = userInputinitialAmount * percentageOfYear / percentageDivider;    
    userInputinitialAmount += pecentageProfitAfterYear;    
}

const totalAmount = userInputinitialAmount;
const totalProfit = userInputinitialAmount - initialAmount;

if (isNaN(initialAmount) || isNaN(numberOfYears) || isNaN(percentageOfYear)) {
    alert('Invalid input data');
}else if (initialAmount < minInitialAmount || numberOfYears < minNumberOfYears 
    || percentageOfYear >maxPercentageOfYear) {
    alert('Invalid input data');
}else {
    alert(`Initial amount: ${initialAmount} 
    Number of years: ${numberOfYears}
    Percentage of year: ${percentageOfYear}

    Total profit: ${totalProfit.toFixed(toFixedtwoDecimalPlaces)}
    Total amount: ${totalAmount.toFixed(toFixedtwoDecimalPlaces)}`)
}
