/* START TASK 1: Your code goes here */

let selectedTd;

let table = document.getElementById('table');

table.onclick = function(event) {
    let td = event.target.closest('td');

    if (!td) {
        return;
    }
    if (!table.contains(td)) {
        return;
    }

    highlight(td);
}

function highlight(td) {

    selectedTd = td;
    selectedTd.classList.toggle('yellow');

}

/* END TASK 1 */

/* START TASK 2: Your code goes here */

/* END TASK 2 */

/* START TASK 3: Your code goes here */

/* END TASK 3 */
