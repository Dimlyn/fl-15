const maxElement = (arr) => Math.max(...arr);



const copyArray = (arr) => [...arr];
const copiedArray = copyArray();

let array;
const unique = () => {
    const result = new Set();
    array.forEach(i => result.add(i))
    return [...result];
}


const hideNumber = (str) => {
    const fourDigits = 4;
    const hidenStr = str.slice(str.length-fourDigits, str.length);
    return hidenStr.padStart(str.length, '*');
}


const add = (...args) => {
    if(args.length <= 0) {
        throw new Error('Missing Property')
    }

    return args.reduce((acc, item) => acc + item);
}


const get = () => new Promise((resolve, reject) => fetch('https://jsonplaceholder.typicode.com/users')
    .then(response => response.json())
    .then(data => {
        const result = data.map(({name}) => name).sort()
        return resolve(result)
    })
    .catch(err => reject(console.log(err)))
)

get().then(i => console.log('RESULT', i))


const asyncAwaitGet = async () => {
    const response = await fetch('https://jsonplaceholder.typicode.com/users')
    const result = await response.json()
    return result;
}

asyncAwaitGet().then(i => console.log('RESULT', i))
