let computerScore = 0;
let userScore = 0;

const lose = -1;
const draw = 0;
const win = 1;
const valueToWin = 3;

const updateScoreboard = () => {
  document.getElementById('user-score').innerHTML = userScore;
  document.getElementById('computer').innerHTML = computerScore;
};

const addWin = () => {
  userScore++;
  updateScoreboard();
};

const addLost = () => {
  computerScore++;
  updateScoreboard();
};

const dropCounts = () => {
  computerScore = 0;
  userScore = 0;
  updateScoreboard();
};
const GAME_VALUES = ['rock', 'paper', 'scissors'];

const generateValue = () =>
  GAME_VALUES[Math.round(Math.random() * (GAME_VALUES.length - 1))];
const isWin = (you, enemy) => {
    if (you === enemy) {
        return draw;

    }else if (you === 'rock' && enemy === 'paper') {
        return lose;

    }else if (you === 'paper' && enemy === 'scissors') {
        return lose;

    }else if (you === 'scissors' && enemy === 'rock') {
        return lose;

    }else {
        return win;  
    }
  
};

const updateMessage = (messsage) => {
  document.getElementById('message').innerHTML = messsage;
};

const compareScores = () => {
  if (userScore === valueToWin) {
    updateMessage('Great success, you won!');
  }
  if (computerScore === valueToWin) {
    updateMessage('You lose, try again');
  }
  if (computerScore === valueToWin || userScore === valueToWin) {
    document.getElementById('rock').setAttribute('disabled', 'disabled');
    document.getElementById('paper').setAttribute('disabled', 'disabled');
    document.getElementById('scissors').setAttribute('disabled', 'disabled');
  }
};
function rock(param) {
  const computerValue = generateValue();
  const iW = isWin('rock', computerValue);
  switch (iW) {
    case lose: 
      updateMessage(`${param} vs. ${computerValue}  -You’ve LOST!`);
      addLost();
      break;
    case draw: 
      updateMessage(`${param} vs. ${computerValue}  - it's a draw!`);
      break;
    case win: 
      updateMessage(`${param} vs. ${computerValue}  -You’ve WON!`);
      addWin();
      break;
    default:
      break;
  }
  compareScores();
}

function reset() {
  dropCounts();
  document.getElementById('rock').removeAttribute('disabled');
  document.getElementById('paper').removeAttribute('disabled');
  document.getElementById('scissors').removeAttribute('disabled');
  updateMessage(`let's start!`);
}

document.getElementById('rock').addEventListener('click', () => rock('rock'));

document.getElementById('paper').addEventListener('click', () => rock('paper'));

document
  .getElementById('scissors')
  .addEventListener('click', () => rock('scissors'));

document.getElementById('reset').addEventListener('click', reset);