const $list = $(".list");
const $input = $("#add-input");
const $add = $("#add-submit");


const todos = [
  {
    text: "Buy milk",
    done: false
  },
  {
    text: "Play with dog",
    done: true
  }
];

function addTodoItem() {
  const todoItem = $input.val();
  $list.append(`<li class="item"><span class="item-text">${todoItem}</span><button class="item-remove">Remove</button>`);
  $list .val("");
}

function deleteTodoItem(e, item) {
  e.preventDefault();
  $(item).parent().remove();
}

function completeTodoItem() {  
  $(this).parent().toggleClass("strike");
}

$(function addTodoItemByClick() {
 
  $add.on('click', function(e){
    e.preventDefault();
    addTodoItem()
  });
})

$(function deleteTodoItemByRemoveBtn() {
  $list.on('click', '.item-remove', function(e){
    var item = this; 
    deleteTodoItem(e, item)
  });
})

$(function strikeOutTodoItem() {
  $(document).on('click', ".item-text", completeTodoItem)
})