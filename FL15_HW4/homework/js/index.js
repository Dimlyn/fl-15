class Deck {
    constructor() {
        this.cards = [];

        const cardSuits = ['Hearts', 'Spades', 'Clubs', 'Diamonds'];
        const cardValues = ['A', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K'];

        for (let cardSuit in cardSuits) {
            if (Object.prototype.hasOwnProperty.call(cardSuits, cardSuit)) {

            
                for (let cardValue in cardValues ) {
                    if (Object.prototype.hasOwnProperty.call(cardSuits, cardSuit)) {
                        this.cards.push(`${cardValues[cardValue]} of ${cardSuits[cardSuit]}`);
                
                    }   
                }
            }
        }

    }
}

const cards1 = new Deck();

console.log(cards1.cards);