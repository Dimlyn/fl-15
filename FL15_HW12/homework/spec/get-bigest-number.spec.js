const getBiggestNumber = require('../src/get-bigest-number');

describe('Get Biggest Number', () => {
    it('should import whole module using module.exports = getBiggestNumber;', async () => {
        const result = getBiggestNumber(5, -100);
        expect(result).toBe(5);
    });

    it('should work properly with negative numbers;', async () => {
        const result = getBiggestNumber(-300, -1);
        expect(result).toBe(-1);
    });

    it('should throw warning on not enough arguments', async () => {
        const result = getBiggestNumber(2);
        expect(result).toBe('Not enough arguments');
    });

    it('should throw warning on args length > 10;', async () => {
        const result = getBiggestNumber(1,2,3,4,5,6,7,8,9,10,11);
        expect(result).toBe('Too many arguments');
    });

    it('should throw error on wrong arg type', async () => {
        try {
        const result = getBiggestNumber('1', null);
        console.log(result);
            expect(result).toThrow('Wrong argument type');
        } catch(err) {
            err;
        }

    });
});